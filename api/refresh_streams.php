<!-- Koden skrevet af Asger Møller -->
<?php
/* The mySQL database, tables and connection from these scripts to that database must be set up with charset utf8mb4.
This is because the utf8 charset in mySQL will only contain characters with a length of 3 bytes, whereas some of the
characters coming from the API (especially some chinese characters in the streamers' titles) will be up to 4 bytes long.*/
include($_SERVER['DOCUMENT_ROOT'] . '/includes/connect_login.php');
include($_SERVER['DOCUMENT_ROOT'] . '/includes/data/api_resources.php');

// Base URL of Twitch api. Information about the object returned by the API call can be found at https://dev.twitch.tv/docs/api/reference#get-streams
$streamsApi = "https://api.twitch.tv/helix/streams";
// clientId for interacting with Twitch API. Client ids are managed at https://dev.twitch.tv/dashboard - Note that this ID is different from what's used in the authentication functionality to reduce request throttling
$clientId = "hn8w18i343n3hl3tx5y3sxk0sh24o2";

$apiToDatabaseDictionary = [
    "user_id" => "twitch_id",
    "type" => "status_id",
    "game_id" => "game_id",
    "title" => "title",
    "viewer_count" => "viewers",
    "started_at" => "start",
    "language" => "language",
    "thumbnail_url" => "preview"];

$timeStart = microtime(true);

// Gets id that's identifiable by Twitch API for all streamers registered locally
$query = "SELECT twitch_id FROM twitch_channel";
$result = mysqli_query($con, $query);
if (!$result) die (mysqli_error($con));
$rows = mysqli_num_rows($result);
if ($rows == 0) echo "No streamers found.<br>";

$allTwitchIds = array();
$onlineCount = 0;
$offlineCount = 0;

// The dictionary must mirror the statuses table of the database
$statusDictionary = [
    "offline" => 0,
    "live" => 1,
    "vodcast" => 2];

// Translates the mysql associative arrays to a single list of all the streamers that are to be refreshed
while ($row = mysqli_fetch_assoc($result)) {
    $allTwitchIds[] = intval($row['twitch_id']);
}

// Separates the streamers in batches of 100, this is because the Twitch.tv API can only process up to 100 lookups per API call
for ($i = 0; $i <= (ceil($rows/100)-1); $i++) {
    // Concatenates user_ids into a single string for the API call
    $twitchIds = "user_id=";
    $twitchIdArray = array();
    for ($ii = 1; (($i*100)+$ii) <= $rows && (($i*100)+$ii) <= (($i+1)*100); $ii++) {
        $twitchIdArray[] = $allTwitchIds[($i*100)+($ii-1)];
    }
    $twitchIds = $twitchIds . implode("&user_id=",$twitchIdArray);
    $responseArray = api_call_simple($streamsApi, $twitchIds);
    
    $insertOnlineValues = array();
    $insertOfflineValues = array();
    $processedIds = array();
    
    // Parse information about each streamer from the decoded JSON object returned by the api_call function and prepare the values for insertion in the mySQL query
    foreach ($responseArray['data'] as $stream) {
        $insertString = "(";
        foreach ($apiToDatabaseDictionary as $apiColumn => $databaseColumn) {
            $rowValue = $stream[$apiColumn];
            if ($apiColumn == 'type') {$insertString .= $statusDictionary[$rowValue];}
            else if (is_string($rowValue)){
                $insertString .= "'";
                if ($apiColumn === 'title') {
                $insertString .= mysqli_real_escape_string($con, $rowValue);
                } else {$insertString .= $rowValue;}
                $insertString .= "'";
            } else {$insertString .= $rowValue;}
            $insertString .= ",";
        }
        $insertString = mb_substr($insertString, 0, -1);
        $insertString .= ")";
        $insertOnlineValues[] = $insertString;
        $processedIds[] = $stream['user_id'];
    }
    
    // Check for streamIds not returned by the api and prepare values for these streamers for insertion in the mySQL query
    $offlineIds = array_diff($twitchIdArray, $processedIds);
    foreach ($offlineIds as $offlineId) {
        $insertString ="(" . 
            $offlineId . "," . 
            0 . "," . 
            0 . ")";
        $insertOfflineValues[] = $insertString;
    }
    
    // If any online streams are returned by the API, insert information about those streams in the twitch_stream table
    if (count($processedIds)>0) {
        $insertOnlineQueryValues = implode(",",$insertOnlineValues);
        $insertOnlineQuery = "INSERT INTO twitch_stream (";
        $insertDuplicates = "";
        foreach ($apiToDatabaseDictionary as $apiColumn => $databaseColumn) {
            $insertOnlineQuery .= $databaseColumn . ",";
            $insertDuplicates .= $databaseColumn . "=VALUES(" . $databaseColumn . "),";
        }
        $insertOnlineQuery = mb_substr($insertOnlineQuery, 0, -1);
        $insertDuplicates = mb_substr($insertDuplicates, 0, -1);
        $insertOnlineQuery .= ") VALUES $insertOnlineQueryValues
        ON DUPLICATE KEY UPDATE $insertDuplicates;";
        $insertOnlineResult = mysqli_query($con, $insertOnlineQuery);
        if (!$insertOnlineResult) die (mysqli_error($con));
    }
    
    // If any offline streams are found, insert information about those streams in the twitch_stream table
    if (count($offlineIds)>0) {
        $insertOfflineQueryValues = implode(",",$insertOfflineValues);
        $insertOfflineQuery = "INSERT INTO twitch_stream (
            twitch_id,
            status_id,
            viewers) 
        VALUES $insertOfflineQueryValues
        ON DUPLICATE KEY UPDATE status_id=VALUES(status_id),viewers=VALUES(viewers);";
        $insertOfflineResult = mysqli_query($con, $insertOfflineQuery);
        if (!$insertOfflineResult) die (mysqli_error($con));
    }
    $onlineCount = $onlineCount + count($processedIds);
    $offlineCount = $offlineCount + count($offlineIds);
}

register_missing_games($con, $clientId);

calculate_game_viewcount($con, $clientId);

$timeEnd = microtime(true);
$executionTime = ($timeEnd - $timeStart);
echo '<br><b>Total Execution Time:</b> '.$executionTime.' seconds.<br>';
echo $onlineCount . "/" . ($onlineCount + $offlineCount) . " streams online";
?>

<script>
// Refreshes page to execute PHP script every 5 minutes. Optimally this script is run as a serverside cron task
setTimeout(function () { window.location.reload(); }, 5*60*1000);
// Shows time of last refresh.
document.write(new Date());
</script>