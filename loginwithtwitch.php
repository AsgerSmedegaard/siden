<?php
// Buffer all output to when the code has ben run to be able to set a new header halfways in the script.
ob_start();
include ($_SERVER['DOCUMENT_ROOT'] . '/includes/data/api_resources.php');
header("Location: https://api.twitch.tv/kraken/oauth2/authorize?client_id=$clientId&redirect_uri=$redirectUrl&response_type=code&state=$clientSecret&scope=$userScopes");
ob_end_flush();
exit();
?>