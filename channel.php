<?php
// Buffer all output to when the code has ben run to be able to set a new header halfways in the script.
ob_start();
include($_SERVER['DOCUMENT_ROOT'] . "/includes/header.php");
include($_SERVER['DOCUMENT_ROOT'] . "/includes/sidebar.php");
include($_SERVER['DOCUMENT_ROOT'] . "/pages/channel_page.php");
include($_SERVER['DOCUMENT_ROOT'] . "/includes/footer.php");
ob_end_flush();
exit();
?>