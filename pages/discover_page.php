<!-- Koden skrevet af Christian Davis -->
<div class="discover_page">

    <div id="streamers">
        
        <?php
        include ($_SERVER['DOCUMENT_ROOT'] . '/includes/data/dbgrab_discover.php');
        /* the following Code is written by Christian Davis */

    if (!isset($_GET['tag']) && !isset($_GET['game'])){
        dbSetupStreamer($con);
    }
        else if (isset($_GET['tag']) && isset($_GET['game'])){
            dbSetupStreamer($con,$_GET['tag'],$_GET['game']);
        } else if (isset($_GET['tag']) && !isset($_GET['game'])){
            dbSetupStreamer($con,$_GET['tag']);
        } else if (!isset($_GET['tag']) && isset($_GET['game'])){
            dbSetupStreamer($con,NULL,$_GET['game']);
        } else{
            echo 'Something is very wrong with the tag params and the $_GET';
        }
        
        echo '<br>';
?>
    </div>
</div>

<script> 
$(document).ready(function(){
    $(".cdStreamerDiv").click(function(){
        $(this).find(".inputFormHidden").submit();
    });
});
</script>