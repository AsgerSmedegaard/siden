<!-- Koden skrevet af Christian Davis -->
<?php
include ($_SERVER['DOCUMENT_ROOT'] . '/includes/data/dbgrab_discover.php');
/* the following Code is written by Christian Davis */
 if (isset($_GET['channel'])) {
    $channel = $_GET['channel'];
} else if (!isset($_GET['channel'])) {
    $randTwitchId = getRandomUserCh($con);
    //Gets a random streamer name based on the random twitch id
    $channel = dbFetchCh($con,$randTwitchId, 'login_name');
} else {
    echo 'Something is very wrong regarding the $_GET';
}


if ($channel != NULL){
$channelName = dbSetupStreamerChannel($con,$channel,'display_name');
$id = dbSetupStreamerChannel($con,$channel,'twitch_id');
} else {
    $channelName = 'No channel was found: Error '.$channel.' = NULL';
}

//setting useful variables below v
$id; //is set to dbSetupStreamerChannel($con,$channel,'twitch_id');
$chId = $channel; //is set to dbFetchCh($con,$randTwitchId, 'login_name');
$chName = dbFetchCh($con, $id, 'display_name'); //get the displayed name from twitch
$game = dbFetchCh($con, $id, 'game_name'); //gets the name of the game streamer is playing
$gameArt = dbFetchCh($con, $id, 'game_art_url'); //gets the art the streamer if playing
$title = dbFetchCh($con, $id, 'title'); //gets the title the streamer has set 
$views = dbFetchCh($con,$id, 'viewers'); //gets the number of current viewers
?>
    <div id="channelPage">
        <div id="channelContent">
            <?php include ($_SERVER['DOCUMENT_ROOT'] . '/includes/data/stream.php'); ?>

            <?php include ($_SERVER['DOCUMENT_ROOT'] . '/includes/data/chat.php'); ?>
        </div>

        <div id="streamInfo">
            <h3>
                <?php e($title) ?>
            </h3>
            <div id="container">
                <button id="toggleChat">Toggle Chat</button>
                <form method="GET" action="discover.php">
                    <input id="imgGame" type="image" name="game" value="<?php e($game) ?>" src="<?php e($gameArt) ?>" title="<?php e($game) ?>" alt="<?php e($chName) ?> is currently playing <?php e($game) ?>" />
                </form>
                <div id="info">
                    <div class="topLeftCornor">
                    </div>
                    <div class="topRightCornor">
                    </div>
                    <div class="botLeftCornor">
                    </div>

                    <div class="botRightCornor">
                    </div>
                    <div id="wrap">
                        <span> <strong><?php e($chName) ?> </strong> is currently playing <strong> <?php e($game) ?></strong> with <strong><?php e($views) ?></strong> current viewers.</span>
                        <br>
                        <div id="descTagWrap">
                            <p id="streamerDescription">Desc goes here</p>
                            <?php dbSetupStreamerGetTagsChannel($con, $id); ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Mangler so far:description, måske sociale medier -->
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $("#toggleChat").click(function() {
                if ($("#channelPage").find("iframe:nth-child(even)").css('display') == 'none') {

                    if ($(".sideBar").css('display') == 'none') {
                        $("#channelPage").find("iframe:nth-child(even)").toggle("slow");
                        $("#channelContent").animate({
                            "height": "35vw"
                        }, 400);
                    } else {
                        $("#channelPage").find("iframe:nth-child(even)").toggle("slow");
                        $("#channelContent").animate({
                            "height": "35vw"
                        }, 400);
                    }

                } else {
                    if ($(".sideBar").css('display') == 'none') {
                        $("#channelPage").find("iframe:nth-child(even)").toggle("slow");
                        $("#channelContent").animate({
                            "height": "56vw"
                        }, 400);
                    } else {
                        $("#channelPage").find("iframe:nth-child(even)").toggle("slow");
                        $("#channelContent").animate({
                            "height": "47vw"
                        }, 400);
                    }
                }
            });
        });




    </script>
