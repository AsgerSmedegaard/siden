<!-- Koden skrevet af: Asger Smedegaard -->
<div class="sideBar" id="sidebar">
    <p id="mobilehint" class="sidebarcollapsable">Show filters</p>
    <script>
        // Toggles the collapsed class on the sidebar for styling with #sidebar.collapsed if sidebar has been toggled off
        if (localStorage.getItem('sidebarToggle') == 1) {
            document.getElementById('sidebar').classList.add('collapsed');
        };

    </script>
    <!-- Icons from Font Awesome free for commercial use -->
    <h2 class="sidebarcollapsable">Tags<img class="h2Img sidebarcollapsable" src="/img/font-awesome_4-7-0_hashtag_48.png"></h2>
    <div id="popTags">
        <div class="sidebarcollapsable" id="reset_button_container">
            <form method="GET">
                <!-- standard form bruger metoden get som ligger sig i url -->
                <input class="sidebaritem sidebarcollapsable" id="reset_button" title="Reset filters" type="submit" name="" value="reset" />
            </form>
        </div>
        <script>
            // This script is used to toggle the class collapsed of HTML elements which are impacted by the sidebar collapsing and inflating (Elements with the class sidebarcollapsable). A local variable is set to remember the position of the sidebar between pages.
            $(document).ready(function() {
                // Executes the following code when the sidebar is clicked.
                $('.sideBar').on('click', function() {
                    // The sidebar should only collapse if the mouse is not on top of any clickable elements in the sidebar. Clickable elements must have the sidebaritem class.
                    if ($('.sidebaritem:hover').length == 0) {
                        if (localStorage.getItem('sidebarToggle') == 0) {
                            document.getElementById('sidebar').classList.toggle('collapsed');
                            var collapsables = document.getElementsByClassName('sidebarcollapsable');
                            [].forEach.call(collapsables, function(collaps) {
                                collaps.classList.toggle('collapsed');
                            });
                            localStorage.setItem('sidebarToggle', 1);
                        } else if (localStorage.getItem('sidebarToggle') == 1) {
                            document.getElementById('sidebar').classList.toggle('collapsed');
                            localStorage.setItem('sidebarToggle', 0);
                            var collapsables = document.getElementsByClassName('sidebarcollapsable');
                            [].forEach.call(collapsables, function(collaps) {
                                collaps.classList.toggle('collapsed');
                            });
                        } else {
                            document.getElementById('sidebar').classList.toggle('collapsed');
                            localStorage.setItem('sidebarToggle', 1);
                            var collapsables = document.getElementsByClassName('sidebarcollapsable');
                            [].forEach.call(collapsables, function(collaps) {
                                collaps.classList.toggle('collapsed');
                            });
                        }
                    }
                });
            });

        </script>

        <?php
        include ($_SERVER['DOCUMENT_ROOT'] . '/includes/data/dbgrab_channel.php');
        $sql = "SELECT tag, tag_icon FROM rates ORDER BY rating DESC LIMIT 9"; //opretter en variabel som stemmer overens med det som skal stå i query funktionen

        $result = $con->query($sql); //opretter varialben result og sætter den til at være = conn hvor vi henter funktionen inde i connection som hedder query og indsætter sql dataene i denne funktion som henter dataene fra sql databasen.

        while($row = $result->fetch_assoc()) //mens row = associate array print beskeden under. dvs den går fra row 1 til row 2 til row 3 osv indtil der kommer et punkt i tabellen hvor der ikke står data da den så er row = null
            
        //beskeden som printes er html php code som printes til html kode som er en form der benyttes til at gemme tags (og display et icon af taggen) samt spillet hvis dette er valgt ved hjælp af isset.
        {
            echo "<div id='".$row['tag']."' title='".$row['tag']."' class='tagContainer sidebarcollapsable'>";
            echo "<form class='tagForm sidebarcollapsable' method='GET' action= " . $baseUrl . "/discover.php>";

            if (isset($_GET['game'])) {
                $tagvalue = $_GET['game'];
                echo '<input type="hidden" name="game" value="'.$tagvalue.'"/>'; 
            }
            echo "<input class='sidebaritem tags sidebarcollapsable' type='submit' value='" . $row['tag'] . "' name='tag' style='background-image: url(".$row['tag_icon'].")'/>";
            echo "</div>";
            echo "</form>";
        }
        ?>
            <!-- The graphics of chill.png and amazing.png are credited to Zee Que of http://www.designbolts.com/ -->
    </div>
    <div id="populargames">
        <h2 class="sidebarcollapsable">Games<img class="h2Img sidebarcollapsable" src="/img/font-awesome_4-7-0_gamepad_48.png"></h2>
        <div class="gamessidebar">
            <form id="gamesform" method='GET' action=' <?php echo "$baseUrl/discover.php"?>'>
                <?php 
                if (isset($_GET['tag'])){
                    $tagvalue = $_GET['tag'];
                    echo '<input type="hidden" name="tag" value="'.$tagvalue.'"/>'; 
                };
                dbGetGamesSidebar($con, TRUE);
                ?>
            </form>
        </div>
    </div>
</div>

<?php
// Inserts JavaScript to add the class chosenFilter to the chosen tags and games for styling in CSS with a special selector
if (isset($_GET['tag'])) {
    $chosenTag = $_GET['tag'];
    echo "<script>";
    echo "document.getElementById('$chosenTag').classList.add('chosenFilter');";
    echo "</script>";
}

if (isset($_GET['game'])) {
    $chosenGame = $_GET['game'];
    echo "<script>";
    echo "document.getElementById('$chosenGame').classList.add('chosenFilter');";
    echo "</script>";
}

function dbGetGamesSidebar($con, $order = NULL) {
    // This function is a slight variation of dbGetGames from dbgrab_discover.php fitted for the purpose of the Sidebar. The main variations are the number of games and the dimensions of the pictures.
    $sS = $sizeSearch = array('{width}','{height}');
    $sR = $sizeReplace = array('90','117');
    
    if ($order != NULL){
        $orderText = "ORDER BY games.game_viewers DESC";
    } else {
        $orderText = NULL;
    }
    
    $query = "SELECT * FROM games WHERE games.game_viewers > 0 $orderText LIMIT 6";
    $result = mysqli_query($con, $query);
    while($row = mysqli_fetch_array($result)){
        $gameName = $row['game_name'];
        $gameArt = $row['game_art_url'];
        
    ?>

    <input id="<?php echo $gameName ?>" class="sidebarcollapsable sidebaritem sidebargame" type="image" name="game" value="<?php echo $gameName ?>" src="<?php echo (str_replace($sS, $sR, $gameArt)) ?>" title="<?php echo $gameName ?>" alt="<?php echo $gameName ?>">
    <?php
    };
}
?>

        <!--    <script>
        skal måske implementeres senere men scriptet går på den ene eller den anden måde i konflikt med noget andet kode uden nogen forklaring.

        var xhttp = new XMLHttpRequest(); //laver variabler, den første er sat = et object som vi kalder senere.
        var method = "GET";
        var url = "/includes/data/data.php")";
        var async = true;

        xhttp.open(method, url, async); //her åbner vi objectet 
        xhttp.send(); //vi sender det som vi åbnede ind i php scriptet

        xhttp.onreadystatechange = function() //når der sker en forandring (når den får data tilbage) i xmlhtttprequest objectet sker dette
        {
            if (this.readyState == 4 && this.status == 200) //readystate 4 = request finished and response is ready. (har den fået svar tilbage) this.status == 200 betyder at operationen er sket uden fejl (dette er meddellelser fra xmlhttprequest som de har scriptet til at sende tilbage)
            {
                document.getElementById("popTags").innerHTML = //tilgår linje 14 inden i curlybrackets
                    "<form method='GET'>" +
                    this.responseText; //laver responded om til text
                "</form>";
            }
        }

    </script>-->

        <main>

            <script>
                // Toggles the collapsed class on all elements with the class sidebarcollapsable for styling with the CSS class collapsed
                if (localStorage.getItem('sidebarToggle') == 1) {
                    var collapsables = document.getElementsByClassName('sidebarcollapsable');
                    [].forEach.call(collapsables, function(collaps) {
                        collaps.classList.add('collapsed');
                    });
                };

            </script>
