<!-- Koden skrevet af Klaus Gregersen -->
<?php
/* Simple script for logging out.
Delete users login token by setting a new cookie with an expiry date in the past
This is to remove the "remember me" persistent login function in auth.php*/
$cookie_name = "login_token";
$cookie_value = "Deleting cookie";
$cookie_expire = time() - 1000;
$cookie_path = "/";
$cookie_domain = "";
$cookie_secure = FALSE;
$cookie_httpOnly = TRUE;
setcookie($cookie_name, $cookie_value, $cookie_expire, $cookie_path, $cookie_domain, $cookie_secure, $cookie_httpOnly);

// Remove all information about the user from the current session
unset($_SESSION['userId']);
unset($_SESSION['twitchId']);
unset($_SESSION['displayName']);
unset($_SESSION['roleId']);
unset($_SESSION['profileImage']);

// Redirect the user to the front page
header("Location: $baseUrl");
?>