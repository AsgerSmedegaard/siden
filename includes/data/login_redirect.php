<!-- Koden skrevet af Asger Møller -->
<?php
/* Is included in connected.php - the landing page of users authorizing login with Twitch
This script acquires accesstokens for validation with Twitch over the OAuth 2 protocol
Firstly, check whether an access code is provided by the response to the initial OAuth grant request and if the client secret returned by the Twitch API matches ours*/
if (isset($_GET['code']) && ($_GET['state'] === $clientSecret)) {
    $codeTemp = $_GET['code'];
    $scopesTemp = $_GET['scope'];
    $defaultRoleId = 1;
    $loginMethod = 1;
    
    // Make a request to the API for an access token via the function api_call set in api_resources
    try {
        $responsearray = api_call("https://api.twitch.tv/kraken/oauth2/token", $clientId, "POST", "client_id=$clientId&client_secret=$clientSecret&code=$codeTemp&grant_type=authorization_code&redirect_uri=$redirectUrl", NULL);
    } catch (Exception $e) {
        echo $e->getMessage();
    }
    
    // Retrieve information from the decoded JSON API response. Set expiry to current server time + "expires in"
    $expiry = time();
    $accessToken = $responsearray['access_token'];
    $refreshToken = $responsearray['refresh_token'];
    $scopesFromApi = $responsearray['scope'];
    $expiry += $responsearray['expires_in'];
    
    // Validate the received token via the cuntion validate_token set in api_resources
    try {
        $tokenValidity = validate_token($accessToken);
    } catch (Exception $e) {
        echo $e->getMessage();
    }
    
    // If the token is valid, attempt to retrieve information about the user with a request to the Twitch API
    if ($tokenValidity === TRUE) {
        // Set the request URL to Twitch API users endpoint.
        $usersApiUrl = "https://api.twitch.tv/helix/users";
        // Request the users information from the API via the function api_call set in api_resources
        try {
            $responsearray = api_call($usersApiUrl, $clientId, "GET", NULL, $accessToken);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        // Navigate the decoded array from the JSON object returned by the API
        $userInfo = $responsearray['data'][0];
        
        // Populate the database with the retrieved user information if there's relevant data in the decoded JSON object
        if ($userInfo['login']) {
            // Check if a user is already registered with the particular Twitch user_id
            $checkUserQuery = "SELECT * FROM users
            WHERE users.twitch_id=" . $userInfo['id'] . ";";
            $checkUserResult = mysqli_query($con, $checkUserQuery);
            if (!$checkUserResult) die (mysqli_error($con));
            
            if (mysqli_num_rows($checkUserResult) < 1) {
                // Create a new user and populate with data if no user was already registered with the particular Twitch user_id
                $insertUsersQuery = "INSERT INTO users
                    (twitch_id,
                    email,
                    login_method,
                    role_id)
                    VALUES (" . $userInfo['id'] . ", '" . 
                    $userInfo['email'] . "',
                    $loginMethod,
                    $defaultRoleId);";
                $insertUsersResult = mysqli_query($con, $insertUsersQuery);
                if (!$insertUsersResult) die (mysqli_error($con));
                $userId = mysqli_insert_id($con);
                if (isset($insertUsersResult) && is_resource($insertUsersResult)) {
                        mysqli_free_result($insertUsersResult);}
            }
            else {
                // If a user was already registered in with the particular Twitch user_id, store his/her internal id in the userId variable
                $userCheck = mysqli_fetch_assoc($checkUserResult);
                $userId = $userCheck['user_id'];
            }
            if (isset($checkUserResult) && is_resource($checkUserResult)) {
                mysqli_free_result($checkUserResult);}
            
            // Definition of a dictionary for easy code maintainance if anything needs to change on our end or anything changes on Twitch's end
            $apiToDatabaseDictionary = [
            "id" => "twitch_id",
            "login" => "login_name",
            "display_name" => "display_name",
            "description" => "description",
            "profile_image_url" => "profile_image",
            "offline_image_url" => "offline_image",
            "view_count" => "view_count",
            "broadcaster_type" => "broadcaster_type"];

            $broadcasterDictionary = [
            "" => 0,
            "affiliate" => 1,
            "partner" => 2];

            // Parse information about the user from the decoded JSON object returned by the api_call function and prepare the values for insertion in the mySQL query utilizing the $apiToDatabaseDictionary with fancy string operations
            $insertUserString = "(";
            foreach ($apiToDatabaseDictionary as $apiColumn => $databaseColumn) {
                $rowValue = $userInfo[$apiColumn];
                if ($apiColumn == 'broadcaster_type') {$insertUserString .= $broadcasterDictionary[$rowValue];}
                else if (is_string($rowValue)){
                    $insertUserString .= "'";
                    if ($apiColumn === 'description' || $apiColumn === 'login' || $apiColumn === 'display_name') {
                    $insertUserString .= mysqli_real_escape_string($con, $rowValue);
                    } else {$insertUserString .= $rowValue;}
                    $insertUserString .= "'";
                } else {$insertUserString .= $rowValue;}
                $insertUserString .= ",";
            }
            $insertUserString = mb_substr($insertUserString, 0, -1);
            $insertUserString .= ")";
            $insertUserValues[] = $insertUserString;

            // Insert information about the new user in the twitch_channel table
            $insertUserQueryValues = implode(",",$insertUserValues);
            $insertUserQuery = "INSERT INTO twitch_channel (";
            $insertDuplicates = "";
            foreach ($apiToDatabaseDictionary as $apiColumn => $databaseColumn) {
                $insertUserQuery .= $databaseColumn . ",";
                $insertDuplicates .= $databaseColumn . "=VALUES(" . $databaseColumn . "),";
            }
            $insertUserQuery = mb_substr($insertUserQuery, 0, -1);
            $insertDuplicates = mb_substr($insertDuplicates, 0, -1);
            $insertUserQuery .= ") VALUES $insertUserQueryValues
            ON DUPLICATE KEY UPDATE $insertDuplicates;";
            $insertUserResult = mysqli_query($con, $insertUserQuery);
            if (!$insertUserResult) die (mysqli_error($con));
            if (isset($insertUserResult) && is_resource($insertUserResult)) {
                mysqli_free_result($insertUserResult);}
            
            // Generate cookie for persistent login in accordance with PIE guidelines available at: https://paragonie.com/blog/2015/04/secure-authentication-php-with-long-term-persistence 
            $selector = base64_encode(random_bytes(10));
            $authenticator = bin2hex(random_bytes(20));
            
            $cookie_name = "login_token";
            $cookie_value =  $selector.':'.base64_encode($authenticator);
            $cookie_expire = time() + 60*60*24*365; // 1 year from now
            $cookie_path = "/";
            $cookie_domain = "";
            $cookie_secure = FALSE;
            $cookie_httpOnly = TRUE;
            
            setcookie($cookie_name, $cookie_value, $cookie_expire, $cookie_path, $cookie_domain, $cookie_secure, $cookie_httpOnly);
            
            // Store information about the users cookie in the database for comparison at automatic user login
            $insertCookieTokenQuery = "INSERT INTO cookie_auth_tokens (user_id,
                cookie_auth_selector,
                cookie_auth_token,
                cookie_auth_expiry)
                VALUES ($userId,
                '$selector', '" .
                hash('sha256', $authenticator) . "',
                $cookie_expire)
                ON DUPLICATE KEY UPDATE cookie_auth_selector=VALUES(cookie_auth_selector),
                cookie_auth_token=VALUES(cookie_auth_token),
                cookie_auth_expiry=VALUES(cookie_auth_expiry);";
            
            // Store information about the users OAuth 2 token in the database for use when the users identity needs validation or we need to interact with the Twitch API on behalf of the user
            $insertOauthTokenQuery = "INSERT INTO oauth_tokens (user_id,
                access_token,
                refresh_token,
                access_token_expiry)
                VALUES ($userId,
                '$accessToken',
                '$refreshToken',
                $expiry)
                ON DUPLICATE KEY UPDATE access_token=VALUES(access_token),
                refresh_token=VALUES(refresh_token),
                access_token_expiry=VALUES(access_token_expiry);";
            
            $insertOauthTokenResult = mysqli_query($con, $insertOauthTokenQuery);
            if (!$insertOauthTokenResult) die (mysqli_error($con));
            $insertCookieTokenResult = mysqli_query($con, $insertCookieTokenQuery);
            if (!$insertCookieTokenResult) die (mysqli_error($con));
            
            // Insert information about the scopes (permissions) associated with the users OAuth2 token authorization
            if ($insertOauthTokenResult === TRUE) {
                if (count($scopesFromApi) > 0) {
                    // Insert information about the scope if it was missing from the scopes table, otherwise retrieves all available scopes and their corresponding IDs.
                    $getScopesQuery = "SELECT * FROM scopes WHERE";
                    foreach ($scopesFromApi as $scope) {
                        $getScopesQuery .= " scope = '" . $scope . "' OR";
                    }
                    $getScopesQuery = substr($getScopesQuery, 0, -3);
                    $getScopesResult = mysqli_query($con, $getScopesQuery);
                    }
                    if (count($scopesFromApi) !== mysqli_num_rows($getScopesResult)) {
                        $insertMissingScopesQuery = "INSERT INTO scopes (scope) VALUES ";
                        foreach ($scopesFromApi as $scope) {
                            $insertMissingScopesQuery .= "('" . $scope . "'),";
                        }
                        $insertMissingScopesQuery = substr($insertMissingScopesQuery, 0, -1);
                        $insertMissingScopesResult = mysqli_query($con, $insertMissingScopesQuery);
                        if ($insertMissingScopesResult === TRUE) {
                            $getScopesResult = mysqli_query($con, $getScopesQuery);
                            while ($row = mysqli_fetch_assoc($getScopesResult)) {
                                $scopeDictionary[$row['scope_id']] = $row['scope'];
                            }
                        }
                    } else {
                        while ($row = mysqli_fetch_assoc($getScopesResult)) {
                            $scopeDictionary[$row['scope_id']] = $row['scope'];
                        }
                    }
                if (isset($getScopesResult) && is_resource($getScopesResult)) {
                    mysqli_free_result($getScopesResult);}
                if (isset($insertMissingScopesResult) && is_resource($insertMissingScopesResult)){
                    mysqli_free_result($insertMissingScopesResult);}
                
                // Store information about which users granted which scopes
                $insertTokenScopesQuery = "INSERT INTO user_scopes (user_id,
                    scope_id)
                    VALUES ";
                foreach ($scopeDictionary as $scopeid => $scope) {
                    $insertTokenScopesQuery .= "(" . $userId . ", " . $scopeid . "),";
                }
                $insertTokenScopesQuery = substr($insertTokenScopesQuery, 0, -1);
                $insertTokenScopesQuery .= ";";
                $insertTokenScopesResult = mysqli_query($con, $insertTokenScopesQuery);
                if (isset($insertTokenScopesResult) && is_resource($insertTokenScopesResult)) {
                    mysqli_free_result($insertTokenScopesResult);
                }
            }
        }
    }
}
?>