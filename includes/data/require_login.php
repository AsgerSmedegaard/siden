<!-- Koden skrevet af Klaus Gregersen -->
<?php
/* Simple script for inclusion in pages where login is required
Checks if a userId is set in session, else prints an error message and ends execution of the script this is included in*/
if(!isset($_SESSION['userId'])) {
    echo ("<div class='errorMessage'>You don't have access to this page, please login.");
    echo ("<p><a class='containsPicture' href='$apiLoginUrl'>Connect with <img class='headerImage' src='/img/twitchicon64x64.png' alt='Twitch'></a></p>");
    echo ("</div>");
    include($_SERVER['DOCUMENT_ROOT'] . "/includes/footer.php");
    die();
}
?>