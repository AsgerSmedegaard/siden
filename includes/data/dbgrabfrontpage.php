<!-- Koden skrevet af Christian Davis & Asger Smedegaard & Klaus Gregersen -->
<?php
include($_SERVER['DOCUMENT_ROOT'] . "/includes/connect_login.php");

//simple function which will echo the content in the parameter

  function br($_ = null) {
            if ($_ > 1) {
                for($i = 0; $i < $_; $i++){
                    echo "<br>";
                }
            }
            else {
                echo "<br>";
            }
  }

function getRandomUser($con) {
    $query ="SELECT 
                twitch_id
            FROM 
                twitch_stream
            WHERE
                status_id = '1'
            ORDER BY RAND()
            LIMIT 1";
    $result = mysqli_query($con, $query);
     while($row = mysqli_fetch_array($result)){
         return $row['twitch_id'];
     };
}

/*
This functions withdraws data, it does this based on the $twitch_id if info is needed for specefic user and/or based on $request if data from a specefic column is need. cxzc
*/
function dbFetch($con, $twitch_id = NULL, $request = NULL){
    $sS = $sizeSearch = array('{width}','{height}');
    $sR = $sizeReplace = array('100','100');
    if ($twitch_id != NULL){
        $whereText =  "twSt.twitch_id = '$twitch_id'";  
    }
    else $whereText = "";
  
     $query ="SELECT * 
    FROM 
        twitch_stream AS twSt
        JOIN games AS ga ON ga.game_id = twSt.game_id
        JOIN twitch_channel AS twCh ON twCh.twitch_id = twSt.twitch_id
    WHERE 
       $whereText
        ";
    
    $result = mysqli_query($con, $query);
    while($row = mysqli_fetch_array($result)){
       if ($request != NULL) {
           return $row[$request];
       } else {
       echo 'You need to set a request and an ID'; 
       }
    };
}

//For testing and setting $_GET based on games in the db
function dbGetGamesFrontpage($con, $order = NULL){
    $sS = $sizeSearch = array('{width}','{height}');
    $sR = $sizeReplace = array('135','175');
    
    if ($order != NULL){
        $orderText = "ORDER BY games.game_viewers DESC";
    } else {
        $orderText = NULL;
    }
    
    $query = "SELECT * FROM games $orderText";
    $result = mysqli_query($con, $query);
    while($row = mysqli_fetch_array($result)){
        $gameName = $row['game_name'];
        $gameArt = $row['game_art_url'];
        $views = $row['game_viewers'];
        
        
        ?>


    <input type="image" name="game" value="<?php e($gameName) ?>" src="<?php e(str_replace($sS, $sR, $gameArt)) ?>" title="<?php e($views)?>" alt="<?php e($gameName) ?>">
    <?php
    };    
}

//Aliases needing Search&Replace: preview, profileImage, gameArt, offlineImage
//This funktion creates the streamer preview, and has been created so it can be used when segmenting based on our tags and or games, with the limiter so it can be used on the frontpage
function dbSetupStreamerFrontpage($con, $tag = NULL, $game = NULL, $limit){
    $sS = $sizeSearch = array('{width}','{height}');
    $sR = $sizeReplace = array('225','126');
    //Due to the way the database is setup it is only possible to filter games directly in the SELECT and not possible to filter tags in SELECT, tags must be done seperately
     if ($game != NULL){
        $conditional1 = "AND ga.game_name ='$game'";  
    } else {
          $conditional1 = "";
    }
    
     if ($tag != NULL){
        $conditional2 = "AND ra.tag ='$tag'";
        $text = "JOIN streamer_info AS stIn ON stIn.twitch_id = twCh.twitch_id
                JOIN rates AS ra ON ra.tag_id = stIn.tag_id ";
    } else {
          $conditional2 = "";
         $text = "";
    }
    
        if ($limit != NULL){
        $limit = "$limit";  
    } else {
          $limit = "10";
    }
   
    $query ="SELECT * 
    FROM 
        twitch_stream AS twSt
        JOIN games AS ga ON ga.game_id = twSt.game_id
        JOIN twitch_channel AS twCh ON twCh.twitch_id = twSt.twitch_id
        $text
    WHERE 
        status_id = '1'
        $conditional2
        $conditional1
        
    ORDER BY
        twSt.viewers DESC
    LIMIT $limit";
    
    $result = mysqli_query($con, $query);
    while($row = mysqli_fetch_array($result)){
    $name = $row['display_name'];   
    $channel = $row['login_name'];
    $preview = str_replace($sS,$sR, $row['preview']);
    $title = $row['title'];
    $viewers = $row['viewers'];
    $gameName = $row['game_name'];
    $id = $row['twitch_id'];
        //This is a test of splitting php and html internally
        //The following is a huge html loop and therefore we close the php tag
        ?>

        <div class="cdStreamerDiv">
            <img class="cdPreviewImg" src="<?php e($preview) ?>" alt="<?php e($title) ?>" title="<?php e($title)?>">
            <div class="cdBottomBox">
                <div class="cdStreamerInformation">
                    <h3 title="<?php e($title) ?>">
                        <?php e($name); ?> <span class="redDot">&nbsp; &bull;</span>
                        <?php e($viewers) ?>
                    </h3>
                    <p title="<?php e($name)?> is playing <?php e($gameName) ?>">
                        <?php e($gameName) ?>
                    </p>
                    <div class="tagPicture">
                        <?php e(dbSetupStreamerGetTags($con,$id)) ?>
                    </div>
                </div>

                <form class="inputFormHidden" type="GET" action='channel.php'>
                    <input class="formHiddenFp" type="hidden" name="channel" value="<?php e($channel) ?>" />
                </form>
            </div>
        </div>

        <?php
           // the html looping part is over so we open the php tag again
    };
}

//This function can be called to set the tags for a specefik streamer
function dbSetupStreamerGetTags($con, $twitchId){
    $sS = $sizeSearch = array('{width}','{height}');
    $sR = $sizeReplace = array('225','126');
    $query ="SELECT * 
    FROM 
        twitch_stream AS twSt
        JOIN games AS ga ON ga.game_id = twSt.game_id
        JOIN twitch_channel AS twCh ON twCh.twitch_id = twSt.twitch_id
        JOIN get_streamer_tags AS stTags ON stTags.twitch_id = twCh.twitch_id
        JOIN rates AS ra ON ra.tag_id = stTags.tag_id 
        WHERE twSt.twitch_id = '$twitchId'
        ORDER BY stTags.streamer_tag_score DESC
        ";
    
    $result = mysqli_query($con, $query);
    //Should return the tags if there are any for the given streamer
    if ($result) {
            while($row = mysqli_fetch_array($result)){
        
        if ($row['tag_active'] == '1'){
            $tag = $row['tag'];
            $img = $row['tag_icon'];
             echo '<div class="cdInternalTagSetup"><img class="cdTagImage" src="'. $img .'" title="'. $tag.'" alt="This tag is: '. $tag.'" /></div>';
        };    
  
    };
        
    };

}
?>
