<!-- Koden skrevet af Christian Davis -->
<?php
//simple function which will echo the content in the parameter
function e($t = NULL){
    echo $t;
}

//For testing and setting $_GET based on games in the db
function dbGetGames($con, $imgXDimension, $imgYDimension){
    $sS = $sizeSearch = array('{width}','{height}');
    $sR = $sizeReplace = array($imgXDimension, $imgYDimension);
    $query = "SELECT * FROM games ORDER BY game_viewers ASC LIMIT 9";
    $result = mysqli_query($con, $query);
    while($row = mysqli_fetch_assoc($result)){
        echo '<input class="sideBarImage" type="image" name="game" value="'.$row['game_name'].'" src="'.str_replace($sS, $sR, $row['game_art_url']).'" alt="Choose Game to filter from">';
    };    
}

//Aliases needing Search&Replace: preview, profileImage, gameArt, offlineImage
//This funktion creates the streamer preview, and has been created so it can be used when segmenting based on our tags and or games
function dbSetupStreamer($con, $tag = NULL, $game = NULL){
    $sS = $sizeSearch = array('{width}','{height}');
    $sR = $sizeReplace = array('225','126');
    //Due to the way the database is setup is is only possible to filter games directly in the SELECt and not possible to filter tags in SELECT, tags must be done seperately
     if ($game != NULL){
        $conditional1 = "AND ga.game_name ='".mysqli_real_escape_string($con, $game)."'";
    } else {
          $conditional1 = "";
    }
    
     if ($tag != NULL){
        $conditional2 = "AND ra.tag ='$tag'";
        $text = "JOIN get_streamer_tags AS stTags ON stTags.twitch_id = twCh.twitch_id
                JOIN rates AS ra ON ra.tag_id = stTags.tag_id ";
    } else {
          $conditional2 = "";
         $text = "";
    }
    
    
    $query ="SELECT * 
    FROM 
        twitch_stream AS twSt
        JOIN games AS ga ON ga.game_id = twSt.game_id
        JOIN twitch_channel AS twCh ON twCh.twitch_id = twSt.twitch_id
        $text
    WHERE 
        status_id = 1
        $conditional2
        $conditional1
        
    ORDER BY
        twSt.twitch_id ASC";
    
    $result = mysqli_query($con, $query);
    while($row = mysqli_fetch_array($result)){
    $name = $row['display_name'];   
    $channel = $row['login_name'];
    $preview = str_replace($sS,$sR, $row['preview']);
    $title = $row['title'];
    $viewers = $row['viewers'];
    $gameName = $row['game_name'];
    $id = $row['twitch_id'];
    $channelPageUrl = "channel_page.php";
        //This is a test of splitting php and html internally
        
        ?>
    <!-- The following is a huge html loop and therefore we close the php tag -->
    <div class="cdStreamerDiv">
        <div class="cdStreamerPreview">
            <img class="cdPreviewImg" src="<?php e($preview) ?>" alt="<?php e($title) ?>" title="<?php e($title)?>">
        </div>
        <div class="cdBottomBox">
            <div class="cdStreamerInformation">
                <h3 title="<?php e($title) ?>">
                    <?php e($name); ?> <span class="redDot">&nbsp; &bull;</span>
                    <?php e($viewers) ?>
                </h3>
                <p title="<?php e($name)?> is playing <?php e($gameName) ?>">
                    <?php e($gameName) ?>
                </p>
                <div class="tagPicture">
                <?php e(dbSetupStreamerGetTagsdiscover($con,$id)) ?>
                </div>
            </div>
            <form class="inputFormHidden" type="GET" action='<?php echo "../channel.php" ?>'>
                <input class="formHiddenDi" type="hidden" name="channel" value="<?php e($channel) ?>" />
            </form>
        </div>
        <!--
    
        currently not in use, but is for sending the information forward
        -->
    </div>
    <!-- the html looping part i over so we open the php tag again -->

    <?php
    }
}

//This is used to get the tags for a specific streamer, on the discover page
function dbSetupStreamerGetTagsDiscover($con, $twitchId)
{
    $sS = $sizeSearch = array('{width}','{height}');
    $sR = $sizeReplace = array('225','126');
    $query ="SELECT * 
    FROM 
        twitch_stream AS twSt
        JOIN games AS ga ON ga.game_id = twSt.game_id
        JOIN twitch_channel AS twCh ON twCh.twitch_id = twSt.twitch_id
        JOIN get_streamer_tags AS stTags ON stTags.twitch_id = twCh.twitch_id
        JOIN rates AS ra ON ra.tag_id = stTags.tag_id
        WHERE 
        twSt.twitch_id = '$twitchId'
        ORDER BY stTags.streamer_tag_score DESC
        ";
    
    if ($result = mysqli_query($con, $query)){
           while($row = mysqli_fetch_array($result)){
        
        if ($row['tag_active'] == '1'){
            $tag = $row['tag'];
            $img = $row['tag_icon'];
             echo '<div class="cdInternalTagSetup"><img class="cdTagImage" src="'. $img .'" title="'. $tag.'" alt="This tag is: '. $tag.'" /></div>';
        };
      
    }; 
    }

}
?>
