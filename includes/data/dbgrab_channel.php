<!-- Koden skrevet af Christian Davis -->
<?php

//Aliases needing Search&Replace: preview, profileImage, gameArt, offlineImage
//This function is used to get the needed data from our database to setup the channel page
function dbSetupStreamerChannel($con, $login_name = NULL, $request = NULL){
    $sS = $sizeSearch = array('{width}','{height}');
    $sR = $sizeReplace = array('100','100');
    if ($login_name != NULL){
        $channel =  "twCh.login_name = '$login_name'";  
    }
    else $channel = "";
  
     $query ="SELECT * 
    FROM 
        twitch_stream AS twSt
        JOIN games AS ga ON ga.game_id = twSt.game_id
        JOIN twitch_channel AS twCh ON twCh.twitch_id = twSt.twitch_id
    WHERE 
       $channel
        ";
    
    $result = mysqli_query($con, $query);
    while($row = mysqli_fetch_array($result)){
       if ($request != NULL) {
           return $row[$request];
       } else {
       var_dumb($row);   
       }
    };
}
//Channel version of a frontpage funktion. This funktion will SELECT the tags for a specefic streamer
function dbSetupStreamerGetTagsChannel($con, $twitchId){
   $sS = $sizeSearch = array('{width}','{height}');
    $sR = $sizeReplace = array('225','126');
    $query ="SELECT * 
    FROM 
        twitch_stream AS twSt
        JOIN games AS ga ON ga.game_id = twSt.game_id
        JOIN twitch_channel AS twCh ON twCh.twitch_id = twSt.twitch_id
        JOIN get_streamer_tags AS stTags ON stTags.twitch_id = twCh.twitch_id
        JOIN rates AS ra ON ra.tag_id = stTags.tag_id 
        WHERE twSt.twitch_id = '$twitchId'
        ORDER BY stTags.streamer_tag_score DESC
        ";
    
    $result = mysqli_query($con, $query);
    //Should return the tags, but only if there are any for the given streamer
    if ($result) {
            while($row = mysqli_fetch_array($result)){
        
        if ($row['tag_active'] == '1'){
            $tag = $row['tag'];
            $img = $row['tag_icon'];
             echo '<div class="cdInternalTagSetup"><img class="cdTagImage" src="'. $img .'" title="'. $tag.'" alt="This tag is: '. $tag.'" /></div>';
        };    
  
    };
        
    };

}

//Copy-Paste from dbgrabFrontpage - Gets a random twitch_id from our database
function getRandomUserCh($con) {
    $query ="SELECT 
                twitch_id
            FROM 
                twitch_stream
            WHERE
                status_id = '1'
            ORDER BY RAND()
            LIMIT 1";
    $result = mysqli_query($con, $query);
     while($row = mysqli_fetch_array($result)){
         return $row['twitch_id'];
     };
}

//Copy-paste from dbgrabFrontpage
/*
This functions withdraws data, it does this based on the $twitch_id if info is needed for specefic user and/or based on $request if data from a specefic column is need.
*/
function dbFetchCh($con, $twitch_id = NULL, $request = NULL){
    $sS = $sizeSearch = array('{width}','{height}');
    $sR = $sizeReplace = array('75','105');
    if ($twitch_id != NULL){
        $whereText =  "twSt.twitch_id = '$twitch_id'";  
    }
    else $whereText = "";
  
     $query ="SELECT * 
    FROM 
        twitch_stream AS twSt
        JOIN games AS ga ON ga.game_id = twSt.game_id
        JOIN twitch_channel AS twCh ON twCh.twitch_id = twSt.twitch_id
    WHERE 
       $whereText
        ";
    
    $result = mysqli_query($con, $query);
    while($row = mysqli_fetch_array($result)){
       if ($request != NULL) {
           return str_replace($sS,$sR, $row[$request]);
       } else {
       echo 'You need to set a request and an ID'; 
       }
    };
}

?>
