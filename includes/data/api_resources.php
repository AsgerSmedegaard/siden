<!-- Koden skrevet af Asger Møller -->
<?php
// Setting of variables for interaction with the Twitch API. Variables are managed at https://dev.twitch.tv/dashboard/apps
$clientId = 'myorkvr01p0u1ucxwj2eayjihr4gp1';
$redirectUrl = 'http://localhost/connected.php'; //http://localhost/connected.php 'http://thestream.guide/connected.php' for live
$clientSecret = 'v4mrmvwkr0hk1mb9y9p98harz7kt5x';
$userScopes = 'user:read:email';

function refresh_games_table($con, $clientId) {
    // Information about the object returned by API calls can be found at https://dev.twitch.tv/docs/api/reference#get-games
    
    // Base URL of Twitch API endpoint
    $gamesApi = "https://api.twitch.tv/helix/games";
    
    // Dictionary for the datatypes. Makes the types of data wanted easily updateable if Twitch decides to return more data about games
    $apiToDatabaseDictionary = [
    "id" => "game_id",
    "name" => "game_name",
    "box_art_url" => "game_art_url"];
    
    // Gets id that's identifiable by Twitch API for all streamers registered locally
    $query = "SELECT DISTINCT game_id FROM twitch_stream";
        $result = mysqli_query($con, $query);
    if (!$result) die (mysqli_error($con));
    $rows = mysqli_num_rows($result);
    $allGameIds = array();
    
    // Translates the mysql associative arrays to a single list of all the streamers to refresh
    while ($row = mysqli_fetch_assoc($result)) {
        $allGameIds[] = $row['game_id'];
    }
    $allGameIds = array_values(array_filter($allGameIds));
    $gameIdCount = count($allGameIds);
    if ($gameIdCount == 0) {
        echo "<br>All games registered";
        exit;
    }
    
    // Separates the games in batches of 100, this is because the Twitch API can only proces up to 100 lookups per API call
    for ($i = 0; $i <= (ceil($rows/100)-1); $i++) {
        // Concatenates user_ids into a single API request
        $gameIds = "";
        $gameIdArray = array();
        for ($ii = 1; ($i*100+$ii) <= $gameIdCount; $ii++) {
            $gameIds .= "id=" . $allGameIds[$i*100+$ii-1] . "&";
            $gameIdArray[] = $allGameIds[$i*100+$ii-1];
        }
        $gameIds = substr($gameIds, 0, -1);
        
        // Make the API call
        $responsearray = api_call_simple($gamesApi, $gameIds);
        $insertGameValues = array();
        
        // Iterate over the $apiToDatabaseDictionary and the API response to concatenate a SQL query
        foreach ($responsearray['data'] as $game) {
            $insertString = "(";
            foreach ($apiToDatabaseDictionary as $apiColumn => $databaseColumn) {
                $rowValue = $game[$apiColumn];
                if (is_string($rowValue)){
                    $insertString .= "'";
                    if ($apiColumn === 'name' || $apiColumn === 'box_art_url') {
                    $insertString .= mysqli_real_escape_string($con, $rowValue);
                    } else {$insertString .= $rowValue;}
                    $insertString .= "'";
                } else {$insertString .= $rowValue;}
                $insertString .= ",";
            }
            $insertString = mb_substr($insertString, 0, -1);
            $insertString .= ")";
            $insertValues[] = $insertString;
        }
        
        // Framework for the SQL query to update and/or insert games in the games table
        if (count($insertValues)>0) {
            $insertQueryValues = implode(",",$insertValues);
            $insertQuery = "INSERT INTO games (";
            $insertDuplicates = "";
            foreach ($apiToDatabaseDictionary as $apiColumn => $databaseColumn) {
                $insertQuery .= $databaseColumn . ",";
                $insertDuplicates .= $databaseColumn . "=VALUES(" . $databaseColumn . "),";
            }
            $insertQuery = mb_substr($insertQuery, 0, -1);
            $insertDuplicates = mb_substr($insertDuplicates, 0, -1);
            $insertQuery .= ") VALUES $insertQueryValues
            ON DUPLICATE KEY UPDATE $insertDuplicates;";
            $insertResult = mysqli_query($con, $insertQuery);
            if (!$insertResult) die (mysqli_error($con));
        }
    }
}

function register_missing_games($con, $clientId) {
    // Base URL of Twitch api. Information about the object returned by API calls can be found at https://dev.twitch.tv/docs/api/reference#get-games
    $gamesApi = "https://api.twitch.tv/helix/games";
    
    // Dictionary for the datatypes. Makes the types of data wanted easily updateable if Twitch decides to return more data about games
    $apiToDatabaseDictionary = [
    "id" => "game_id",
    "name" => "game_name",
    "box_art_url" => "game_art_url"];
    
    // Selects game_ids not in both the table of current streams and the table of registered games
    $query = "SELECT DISTINCT twitch_stream.game_id FROM twitch_stream LEFT JOIN games ON twitch_stream.game_id = games.game_id
    WHERE games.game_id IS NULL;";
    $result = mysqli_query($con, $query);
    if (!$result) die (mysqli_error($con));
    $rows = mysqli_num_rows($result);
    $allGameIds = array();
    
    // Translates the mysql associative arrays to a single list of all the games returned. These games are missing from the games table
    while ($row = mysqli_fetch_assoc($result)) {
        $allGameIds[] = $row['game_id'];
    }
    $allGameIds = array_values(array_filter($allGameIds));
    $gameIdCount = count($allGameIds);
    if ($gameIdCount == 0) {
        echo "<br>All games registered";
    } else {
        // Separates the games in batches of 100, this is because the Twitch API can only process up to 100 lookups per API call
        for ($i = 0; $i <= (ceil($rows/100)-1); $i++) {
            // Concatenates user_ids into a single API request
            $gameIds = "";
            $gameIdArray = array();
            for ($ii = 1; ($i*100+$ii) <= $gameIdCount; $ii++) {
                $gameIds .= "id=" . $allGameIds[$i*100+$ii-1] . "&";
                $gameIdArray[] = $allGameIds[$i*100+$ii-1];
            }
            $gameIds = substr($gameIds, 0, -1);
            
            // Make the API call
            $responsearray = api_call_simple($gamesApi, $gameIds);
            $insertValues = array();
            
            // Iterate over the $apiToDatabaseDictionary and the API response to concatenate a SQL query
            foreach ($responsearray['data'] as $game) {
                $insertString = "(";
                foreach ($apiToDatabaseDictionary as $apiColumn => $databaseColumn) {
                    $rowValue = $game[$apiColumn];
                    if ($apiColumn === 'id') {$insertString .= $rowValue;
                    } else if (is_string($rowValue)){
                        $insertString .= "'";
                        if ($apiColumn === 'name' || $apiColumn === 'box_art_url') {
                        $insertString .= mysqli_real_escape_string($con, $rowValue);
                        } else {$insertString .= $rowValue;}
                        $insertString .= "'";
                    } else {$insertString .= $rowValue;}
                    $insertString .= ",";
                }
                $insertString = mb_substr($insertString, 0, -1);
                $insertString .= ")";
                $insertValues[] = $insertString;
            }
            
            // Framework for the SQL query to or insert the new games in the games table
            if (count($insertValues)>0) {
                $insertQueryValues = implode(",",$insertValues);
                $insertQuery = "INSERT INTO games (";
                $insertDuplicates = "";
                foreach ($apiToDatabaseDictionary as $apiColumn => $databaseColumn) {
                    $insertQuery .= $databaseColumn . ",";
                    $insertDuplicates .= $databaseColumn . "=VALUES(" . $databaseColumn . "),";
                }
                $insertQuery = mb_substr($insertQuery, 0, -1);
                $insertDuplicates = mb_substr($insertDuplicates, 0, -1);
                $insertQuery .= ") VALUES $insertQueryValues
                ON DUPLICATE KEY UPDATE $insertDuplicates;";
                echo $insertQuery;
                $insertResult = mysqli_query($con, $insertQuery);
                if (!$insertResult) die (mysqli_error($con));
            }
        }
    }
}

function calculate_game_viewcount($con, $clientId) {
    // Gets game_id and current viewers of all streams
    $query = "SELECT game_id, viewers FROM twitch_stream;";
    $result = mysqli_query($con, $query);
    if (!$result) die (mysqli_error($con));
    
    $gameViews = array();
    // Translates the mysql associative arrays to a dictionary of all games adding the viewers of streamers playing the same game together
    while ($row = mysqli_fetch_assoc($result)) {
        if (isset($gameViews[$row['game_id']])) {
            $gameViews[$row['game_id']] += $row['viewers'];
        } else {
            $gameViews[$row['game_id']] = $row['viewers'];
        }
    }
    if (isset($result) && is_resource($result)) {
        mysqli_free_result($result);}
    
    $query = "SELECT game_id FROM games WHERE game_viewers > 0;";
    $result = mysqli_query($con, $query);
    if (!$result) die (mysqli_error($con));
    
    while ($row = mysqli_fetch_assoc($result)) {
        if (!isset($gameViews[$row['game_id']])) {
            $gameViews[$row['game_id']] = 0;
        }
    }
    if (isset($result) && is_resource($result)) {
        mysqli_free_result($result);}
    
    // Update the current amount of viewers on a game from the dictionary created above
    foreach ($gameViews as $gameId => $viewers) {
        $query = "UPDATE games
        SET game_viewers = $viewers
        WHERE game_id = $gameId;";
        $result = mysqli_query($con, $query);
        if (!$result) die (mysqli_error($con));
    }
    if (isset($result) && is_resource($result)) {
        mysqli_free_result($result);}
}

// Receive a fresh access token from the Twitch API
// This function is written in compliance with guidelines at https://dev.twitch.tv/docs/authentication
function refresh_token(int $userId, $con, $refreshToken) {
    // Only the first 2 parameters are required
    global $clientId;
    global $clientSecret;
    
    // refreshToken can optionally be specified to circumvent this initial database lookup
    if ($refreshToken == NULL) {
        $query = "SELECT refresh_token FROM oauth_tokens WHERE user_id=" . $userId . ";";
        $result = mysqli_query($con, $query);
        $rows = mysqli_num_rows($result);
        if ($rows = 1) {
            while ($row = mysqli_fetch_assoc($result)) {
                $refreshToken = $row['refresh_token'];
            }
        } else if ($rows > 1) {
            throw new Exception("User ID: $userId has multiple tokens.");
        } else if ($rows < 1) {
            throw new Exception("No token for user: $userId found.");
        }
        if (isset($result) && is_resource($result)) {
            mysqli_free_result($result);}
    }
    $expiry = time();
    
    // call the Twitch API with a refresh grant and the refresh token requesting a new access token. The required Clientid and client secret are also included
    $encodedUrl = urlencode("https://api.twitch.tv/kraken/oauth2/token?grant_type=refresh_token&refresh_token=$refreshToken&client_id=$clientId&client_secret=$clientSecret");
    try {
        $responseArray = api_call($encodedUrl, $clientId, "POST", NULL, NULL);
    } catch (Exception $e) {
        throw new Exception($e->getMessage());
    }
    $expiry += $responseArray['expires_in'];
    
    // Update information about the token in the oauth_tokens table with the response 
    $query = "UPDATE oauth_tokens
        SET access_token ='" . $responseArray['access_token'] . "',
        refresh_token ='" . $responseArray['refresh_token'] . "',
        expiry = $expiry
        WHERE user_id=" . $userId . ";";
    $result = mysqli_query($con, $query);
    if (isset($result) && is_resource($result)) {
        mysqli_free_result($result);}
    // Return the fresh access token
    $accessToken = $responseArray['access_token'];
    return $responseArray['access_token'];
}

function api_call($apiUrl, $clientId, $requestType, $arguments, $accessToken) {
    // Only the first 3 parameters are required. The rest should be set to NULL if no argument or accessToken is given
    $request = curl_init();
    // Settings for curl API call. The switch is for handling the optional arguments and differences in header syntax based on the Twitch API version being interacted with
    switch ([$arguments, $accessToken]) {
        case [TRUE, TRUE]:
            if (strpos($apiUrl, "helix") === TRUE) {
                curl_setopt_array($request, array(
                    CURLOPT_URL => $apiUrl . "?" . $arguments, 
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => $requestType,
                    CURLOPT_HTTPHEADER => array(
                    'Client-ID: ' . $clientId,
                    'Authorization: Bearer ' . $accessToken)
                ));
            } else if (strpos($apiUrl, "kraken") === TRUE) {
                curl_setopt_array($request, array(
                    CURLOPT_URL => $apiUrl . "?" . $arguments, 
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => $requestType,
                    CURLOPT_HTTPHEADER => array(
                    'Client-ID: ' . $clientId,
                    'Authorization: OAuth ' . $accessToken)
                ));
            }
            break;
            
        case [TRUE, FALSE]:
            curl_setopt_array($request, array(
                CURLOPT_URL => $apiUrl . "?" . $arguments, 
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => $requestType,
                CURLOPT_HTTPHEADER => array(
                'Client-ID: ' . $clientId)
            ));
            break;
            
        case [FALSE, TRUE]:
            if (strpos($apiUrl, "helix") === TRUE) {
                curl_setopt_array($request, array(
                    CURLOPT_URL => $apiUrl,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => $requestType,
                    CURLOPT_HTTPHEADER => array(
                    'Client-ID: ' . $clientId,
                    'Authorization: Bearer ' . $accessToken)
                ));
            } else if (strpos($apiUrl, "kraken") === TRUE) {
                curl_setopt_array($request, array(
                    CURLOPT_URL => $apiUrl,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => $requestType,
                    CURLOPT_HTTPHEADER => array(
                    'Client-ID: ' . $clientId,
                    'Authorization: OAuth ' . $accessToken)
                ));
            }
            break;
            
        case [FALSE, FALSE]:
            curl_setopt_array($request, array(
                CURLOPT_URL => $apiUrl,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => $requestType,
                CURLOPT_HTTPHEADER => array(
                'Client-ID: ' . $clientId)
            ));
            break;
    }
    // Execute API call
    $response = curl_exec($request);
    $err = curl_error($request);
    curl_close($request);
    if ($err === TRUE) {
        throw new Exception("$err");
    }
    if ($response != TRUE) {
        throw new Exception('API response empty');
    }
    // Decode the JSON object returned by the API to an array and return it
    $responseArray = json_decode($response, true);
    return $responseArray;
}

// Flow for validating a user incorporating the refresh_token and validate_token functions
// This function is written in compliance with guidelines at https://dev.twitch.tv/docs/authentication
function validate_user($userId, $con) {
    // Check that the user logs in OAuth 2 on Twitch
    $checkUserQuery = "SELECT login_method FROM users
    WHERE user_id=" . $userId . ";";
    $checkUserResult = mysqli_query($con, $checkUserQuery);
    if (!$checkUserResult) die (mysqli_error($con));
    $row = mysqli_fetch_assoc($checkUserResult);
    if (isset($checkUserResult) && is_resource($checkUserResult)) {
        mysqli_free_result($checkUserResult);}
    if ($row['login_method'] == 1 ) {
        // Retrieve information about the users tokens from the oauth_table
        $getTokenQuery = "SELECT * FROM oauth_tokens
        WHERE user_id=" . $userId . ";";
        $getTokenResult = mysqli_query($con, $getTokenQuery);
        if (!$getTokenResult) die (mysqli_error($con));
        $row = mysqli_fetch_assoc($getTokenResult);
        if (isset($getTokenResult) && is_resource($getTokenResult)) {
        mysqli_free_result($getTokenResult);}
        
        $accessToken = $row['access_token'];
        $refreshToken = $row['refresh_token'];
        $accessExpiry = $row['access_token_expiry'];
        
        // Refresh/validation flow. If the current time is more than the expirydate, executes the first block, else executes the second block
        if (time() > $accessExpiry) {
            // Refresh the accesstoken and check its validity.
            try {
                $accessToken = refresh_token($userId, $con, $refreshToken);
                $tokenValidity = validate_token($accessToken);
            } catch (Exception $e) {
                throw new Exception($e->getMessage());
            }
        } else {
            // Check the accesstokens validity. If the accesstoken is not valid refresh it and validate it again.
            try {
                $tokenValidity = validate_token($accessToken);
                if ($tokenValidity !== TRUE) {
                    $accessToken = refresh_token($userId, $con, $refreshToken);
                    $tokenValidity = validate_token($accessToken);
                }
            } catch (Exception $e) {
                throw new Exception($e->getMessage());
            }
        }
        // Return TRUE if a valid token could be produced, else return FALSE
        if ($tokenValidity == TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}

// Simple function for making an API call. Should be deprecated
function api_call_simple($apiUrl, $arguments) {
    global $clientId;
    
    $request = curl_init();
    // Settings for curl API call
    curl_setopt_array($request, array(
        CURLOPT_URL => $apiUrl . "?" . $arguments . "&first=100",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
        'Client-ID: ' . $clientId)
    ));
    // Execute API call
    $response = curl_exec($request);
    $err = curl_error($request);
    curl_close($request);
    // Decode the JSON object returned by the API to an array and return it
    $responsearray = json_decode($response, true);
    return $responsearray;
}

// Validate a users access token with the Twitch Authentification server
// This function is written in compliance with guidelines at https://dev.twitch.tv/docs/authentication
function validate_token(string $accessToken) {
    global $clientId;
    global $clientSecret;
    
    // Make an API call to the API root supplying the token using the api_call function
    try {
        $responseArray = api_call("https://api.twitch.tv/kraken", $clientId, "GET", NULL, $accessToken);
    } catch (Exception $e) {
        throw new Exception($e->getMessage());
    }
    // Access the relevant response in the returned decoded JSON object and return TRUE if the token is valid and FALSE if not.
    if (($responseArray['identified'] === TRUE) && ($responseArray['token']['valid'] === TRUE)) {
        return TRUE;
    } else {
        return FALSE;
    }

    /* Sample response from Twitch root API validation call when validation succeeds:
    
    {"identified":true,
    "token":{
        "valid":true,
        "authorization":{
            "scopes":["user:read:email"],
            "created_at":"2017-11-07T14:58:06Z",
            "updated_at":"2017-11-07T14:58:06Z"},
        "user_name":"mercilexe",
        "client_id":"abcdefghijklmnopqrstuvxyz12345"
        },
    "_links":{
        "channel":"https://api.twitch.tv/kraken/channel",
        "chat":"https://api.twitch.tv/kraken/chat/mercilexe",
        "teams":"https://api.twitch.tv/kraken/teams",
        "user":"https://api.twitch.tv/kraken/user",
        "users":"https://api.twitch.tv/kraken/users/mercilexe",
        "streams":"https://api.twitch.tv/kraken/streams/mercilexe",
        "ingests":"https://api.twitch.tv/kraken/ingests",
        "channels":"https://api.twitch.tv/kraken/channels/mercilexe"
    }}
    
    When validation fails:
    
    {"identified":true,
    "token":{
        "valid":false,
        "authorization":null},
    "_links":{
        "channel":"https://api.twitch.tv/kraken/channel",
        "teams":"https://api.twitch.tv/kraken/teams",
        "user":"https://api.twitch.tv/kraken/user",
        "streams":"https://api.twitch.tv/kraken/streams",
        "ingests":"https://api.twitch.tv/kraken/ingests"}}*/
}
?>