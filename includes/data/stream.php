<!-- Koden skrevet af Christian Davis -->
<?php
// her embeddes en stream fra twitch på $channel parameteren. Streamen er en iframe.
echo <<< EOT
    <!-- Add a placeholder for the Twitch embed -->
    <div id="twitch-embed"></div>
    <!-- Load the Twitch embed script -->
    <script src="https://embed.twitch.tv/embed/v1.js"></script>
    <!--
      Create a Twitch.Embed object that will render
      within the "twitch-embed" root element.
    -->
    <script type="text/javascript">
      var embed = new Twitch.Embed("twitch-embed", {
        width: "100%",
        height: "100%",
        channel: "$channel",
        layout: "video",
        autoplay: false
      });
      embed.addEventListener(Twitch.Embed.VIDEO_READY, () => {
        var player = embed.getPlayer();
        player.play();
      });
    </script>
EOT;
?>