<!-- Koden skrevet af: Mikkel Kokholm -->
<?php
$baseUrl = "//localhost"; // "//thestream.guide" for live ellers //localhost
// Starts a session if none is set.
if(!isset($_SESSION)) {
    session_start();
}
// Runs the auth document to check a users login status and log users with a persistent log in cookie set into the site.
include_once($_SERVER['DOCUMENT_ROOT'] . '/includes/auth.php');
// Dynamically set page title based on the name of the open document
$page = basename($_SERVER['PHP_SELF']);
switch($page)
{
    case 'index.php':
        $title = 'Welcome to thestream.guide';
        break;
    case 'channel.php':
        $title = 'Channel page';
        break;
    case 'discover.php':
        $title = 'Discover Streamers';
        break;
    case 'profile.php':
        $title = 'Profile and stream management';
        break;
    case 'connected.php':
        $title = 'Connected to Twitch.tv!';
        break;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $title; ?></title>
    <meta charset="UTF-8">
    <meta name="Description" content="Welcome to TheStream.Guide">
    <meta name="keywords" content="Streaming Games">
    <meta name="author" content="Gruppe 2 LOOP 3">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/styles/davis.css">
    <link rel="stylesheet" type="text/css" href="/styles/normalize.css">
    <link rel="stylesheet" type="text/css" href="styles/navcss.css?">
    <script src="https://code.jquery.com/jquery-latest.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
</head>
<body>
    <header>
        <!-- Navigation bar start -->
        <div class="nav">
            <div class="box home">
            <a href="<?php echo $baseUrl ?>">Thestream.guide
            <span><img src="img/husg.png" alt="home"/></span></a>
            </div>
            <div class="box discover">
            <a href="<?php echo $baseUrl ?>/discover.php">Discover
            <span><img src="img/globeg.png" alt="discover"/></span></a>
            </div>
            <div class="box channel">
            <a href="<?php echo $baseUrl ?>/channel.php">Channel
            <span><img src="img/tvg.png" alt="channel"/></span></a> <!-- ikoner er hentet fra denne side hvor de er gratis https://www.iconfinder.com/-->
            </div>
            <div class="box connect">
            <?php if (isset($_SESSION['userId'])) {
                // Sets either a profile icon and profile name or a login button depending on the users login status in the Session variable
                $profileImage = $_SESSION['profileImage'];
                if (is_null($profileImage)) {
                    $profileImage = ($baseUrl . "/img/standardprofileimage.jpg");
                }
                $displayName = $_SESSION['displayName'];
                echo '<a href="'.$baseUrl.'/profile.php">Hi '.$displayName.'!<span><img class="navprofileimage" src="'.$profileImage.'" alt="User Profile Image"/></span></a>';
            } else {
                echo '<a href="'.$baseUrl.'/loginwithtwitch.php">Connect with<span><img src="img/Glitch_White_RGB.png" alt="lol"/></span></a>"';
            } ?>
            </div>
        </div>
    </header>